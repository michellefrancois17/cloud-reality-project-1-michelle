# Cloud Reality Project 1 michelle


## Programmatic remediation using Config rules to detect non-compliant (public) S3 buckets via Lamda_function.
### **Assuming AWS S3 buckets have been created and 1 or more have permissions set to public access.**


### 1. **AWS Config service (create a config rule to search for public S3 bucket and return a list of non-compliant (public) buckets)**

	1. Go to Dashboard, go to rules
	2.  Select Add rule
	3. Select rule type: Add AWS managed rule
	4. Search AWS managed Rules (202) for: S3
 		1. Select Rule Name: s3-bucket-level-public-access-prohibited
		2. Click: Next 
	5. Configure Rule 
		1. Details, Review Description to confirm the rule is NON_COMPLIANT if an Amazon S3 bucket level settings are public. 
		2. Trigger: Select Resources confirm AWS S3 Bucket is a listed resource type
		3. Click: Next
	6. Review: Details, Trigger and Parameters
		1. Click: Add rule
	


### 2. **Lambda Service (create a Lambda Function with and invocation/frequency to search for the config rule with non-compliant S3 buckets, remediate and send an SNS after remediation)**
	1. Go to Dashboard,
	2. Select: Create Function
		1. Select: Author from scratch
		2. Basic information: 
			1. Function name: Enter Function name that describes the purpose of the 
			2. Runtime: Choose Python3.9 from the dropdown
			3. Architecture: Select x86_64
			4. Permissions: ***Create IAM console role 
			5. Select Use an existing role, select the role created for this Lambda function
		3. Click: Create function
	3. Function Overview:
		1. + Add Trigger: Create a Trigger to set the Lambda_function invocation/frequency
		2. + Add Destination: If applicable enter destination
	4. Code Source:
		1. Code tab: Copy/paste or type syntax for Lambda_function 
		2. Use the API Boto3 Library to look for use case documentation for the Lambda_function remediation 
		3. Search google: (Boto3 + Config) (Boto3 + S3) (Boto3 + SNS)
			- (https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/config.html)
			- (https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html)
			- (https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/sns.html )

		4. Import each boto3 client for the respective services
		- Best practice: rename the client to reflect the service 
		5. Search for the use case documentation under available methods
		6. Copy/Paste Code Syntax with get, put and publish functions
		7. Modify the syntax in the Lambda_function, Deploy then Execute. Debug accordingly repeat the process until the execution return successful
		8. Return to Config to confirm the non-compliant S3 buckets are remediated 
			- Note  Config API might be slow to respond



### 3. **IAM Console**
	1. Go to Dashboard, go to Roles
	2. Select: Create Role
	3. Select type of trust entity: AWS Service
		1. Chose case: Lambda
		2. Click: Next: Permissions 
		3. Attach permissions policy
			1. Create Policy or search for an existing policy 
			2. Create IAM console role 
				- with policies that allow Lambda to access the in scope services (config, S3 and SNS)
				- Best practice is to give specific permissions or least permissions necessary
			4. Click Next: Tags
	4. Click: Next Review
		1. Enter Role name 
		2. Review: Role description, Trusted entities, Policies and Permission boundary 
	5. Click: Create role 



### 4. **SNS**
	1. Go to Dashboard, go to Topic
	2. Select: Create topic
		1. Select: Standard 
		2. Details: Name Topic
		3. Click: Create Topic 
	3. Subscriptions: Create Subscription 
		1. Protocol: email 
		2. Enter email address for subscription
		3. Click Create subscription. Note the status is pending confirmation. Confirm by opening the email sent to the designated email address. 
	4. Copy topic ARN into Lambda_funtion publish syntax. 

### 5. **Architecture**
<details><summary>Click to expand</summary>
![img.png](cr_1image.PNG)
</details>

