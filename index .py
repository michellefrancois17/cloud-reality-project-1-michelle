import json
import boto3
s3_client = boto3.client("s3")
config_clinet = boto3.client("config")
SNS_client = boto3.client('sns')
def lambda_handler(event, context):
    response = config_clinet.get_compliance_details_by_config_rule(
    ConfigRuleName='s3-bucket-level-public-access-prohibited',
    ComplianceTypes=['NON_COMPLIANT',])
    non_compliant = json.loads(json.dumps(response, default=str))
    eval_result=non_compliant.get('EvaluationResults')
    for item in eval_result:
        non_comp_bucket = item.get('EvaluationResultIdentifier').get('EvaluationResultQualifier').get('ResourceId')
        print(non_comp_bucket)
        #block the bucket
        response = s3_client.put_public_access_block(
        Bucket= non_comp_bucket,
        PublicAccessBlockConfiguration={
            "BlockPublicAcls": True,
            "IgnorePublicAcls": True,
            "BlockPublicPolicy": True,
            "RestrictPublicBuckets": True 
        })
# def policyNotifier(bucketName, s3client):
#     try:
#         bucketPolicy = s3_client.get_bucket_policy(Bucket = bucketName)
        # notify that the bucket policy may need to be reviewed due to security concerns
        sns_client = boto3.client("sns")
        # subject = "Potential compliance violation in " + bucketName + " bucket policy"
        # message = "Potential bucket policy compliance violation. Please review: " + json.dumps(bucketPolicy['Policy'])
        # send SNS message with warning and bucket policy
        response = sns_client.publish(
            # TopicArn = 'arn:aws:sns:us-east-1:421533670749:s3notificationpublic',
            # Subject = 'Public buckets found',
            # Message = 'Lambda funtion found a public bucket then changed to private'
        )
    # except ClientError as e:
        # error caught due to no bucket policy
        print("No bucket policy found; no alert sent.")
